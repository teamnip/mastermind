import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;



public class MasterMindMain {

   public static int     counter = 0;
   public static Scanner stdin   = new Scanner(System.in);

   public static void main(String[] args) {
      char[] result = null;
      int[] numberToGuess = randomGenerator();
      while (true) {

         System.out
               .println(numberToGuess[0] + " " + numberToGuess[1] + " " + numberToGuess[2] + " " + numberToGuess[3]);

         while (result(result) == false || counter < 10) {
            int[] guessedNumber = getUserInput();
            result = check(guessedNumber, numberToGuess);
            counter++;
         }

         System.out.println("\nErneut spielen? (y/n)");
         String answer = stdin.next().toUpperCase();

         switch (answer) {
         case "Y":
         case "SI":
         case "YE":
         case "YES":
         case "YEAH":
            counter = 0;
            result = null;
            numberToGuess = randomGenerator();
            break;

         default:
            System.out.println("Goodbye!");

            stdin.close();
            System.exit(1);
         }
      }
   }

   /**
    * Generates the random number array for the Mastermind game. Will always be
    * the length of four, including each with a number from zero to nine
    * 
    * @author Maximilian Scopp <m.scopp32 at gmail.com>
    * @return int[4]
    */

   public static int[] randomGenerator() {

      int[] randomNumberList = new int[4];
      do {
         for (int i = 0; i < randomNumberList.length; i++) {
            randomNumberList[i] = new Random().nextInt(10);
         }
      } while (validator(randomNumberList) == false);
      return randomNumberList;
   }

   /**
    * This tool check if the array is right. The user input but also the random
    * number. The number is right, if no number is two times in an array.
    * 
    * @author Timon Zipkat
    * @param givenArray
    * @return boolean
    */

   public static boolean validator(int givenArray[]) {
      for (int i = 0; i < 4; i++) {
         for (int j = 0; j < 4; j++) {
            if (i != j) {
               if (givenArray[i] == givenArray[j]) { return false; }
            }
         }
      }
      return true;
   }

   /**
    * Asks the user for a number input and parses it to an proper four-long
    * integer array. Implies the validator() function for proper checking.
    * 
    * @author Maximilian Scopp <m.scopp32 at gmail.com>
    * @return int[4]
    */
   public static int[] getUserInput() {
      char[] inputNumber = new char[4];
      int[] number = new int[4];
      boolean flag = true;
      do {
         System.out.println("Geben Sie eine vierstellige Zahl ein, ");
         System.out.print("in der keine Zahl doppelt verwendet wird: ");
         inputNumber = String.valueOf(stdin.nextInt()).toCharArray();
         if (inputNumber.length < 5) {
            for (int i = 0; i < number.length; i++) {
               if (i < inputNumber.length) {
                  number[i] = Integer.parseInt(String.valueOf(inputNumber[i]));
               } else {
                  number[i] = 0;
               }
               if (validator(number) == true) {
                  flag = false;
               }
            }
         } else {
            System.out.println("Die eingegebene Zahl darf nicht lÃ¤nger als 4 stellen sein!");
         }
      } while (flag == true);
      return number;
   }

   /**
    * Checks if the two given arrays are the same, if returns an array depending
    * on there match or non-match. (T => Direct Hit | I => Indirect Hit | N =>
    * No hit )
    * 
    * @author Johannes Konze
    * @param userArray
    * @param randomArray
    * @return returnArray
    */

   public static char[] check(int userArray[], int randomArray[]) {
      char returnArray[] = new char[4];

      for (int i = 0; i < 4; i++) {
         returnArray[i] = 'N';
         for (int j = 0; j < 4; j++) {
            if (userArray[i] == randomArray[j]) {
               if (i == j) {
                  returnArray[i] = 'T';
               } else {
                  returnArray[i] = 'I';
               }
            }
         }
      }
      return returnArray;
   }

   /**
    * Checks if the check fx returned an array that indicates a win. If it does
    * congratulates them otherwise displays there result. (T => Direct Hit | I
    * => Indirect Hit | N => No hit )
    * 
    * @author Johannes Konze
    * @param resultArray
    * @return boolean
    */
   public static boolean result(char[] resultArray) {
      char success[] = { 'T', 'T', 'T', 'T' };
      if (resultArray != null) {
         if (Arrays.equals(resultArray, success)) {
            System.out.println("Herzlichen Glückwunsch du hast nach " + counter + " Zügen gewonnen");
            counter = 69;
         } else {
            System.out.println("Deine erste Zahl war ein " + resultArray[0]);
            System.out.println("Deine zweite Zahl war ein " + resultArray[1]);
            System.out.println("Deine dritte Zahl war ein " + resultArray[2]);
            System.out.println("Deine vierte Zahl war ein " + resultArray[3]);
         }
         return true;
      } else {
         return false;
      }
   }
}